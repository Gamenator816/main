
# Comunidad

## Miembros

Aunque GuayaHack es una iniciativa iniciada por {doc}`/community/member/jdsalaro/index`, es un esfuerzo colaborativo de tod@s para tod@s, éstos son sus miembros.

## Voluntarios

### Moderadores

Todos los moderadores son tutores.

```{postlist}
:tags: moderador
:category: miembros
```

%por ahora no es muy claro cual es el rol y quién es tutor
%pues todos los moderadores son tutores y no hay tutores
%que no sean moderadores. Por ahora dejaré ésta sección comentada.

%### Tutores

%Todos los moderadores son participantes.

%```{postlist}
%:tags: tutor
%:category: miembros
%```

### Participantes

```{postlist}
:tags: participante
:category: miembros
```
